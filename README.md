# Icy Coding Practice 

![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

This website provides **the iciest** coding practices for collaborative data-driven / analytical research projects and software development. 

Documentation can be [found here (*link*)](https://thru-echoes.gitlab.io/echoes-coding/).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [I. Software Carpentry: Documention Station!!](#i-software-carpentry-documention-station)
- [II. Documentation is High Priority!](#ii-documentation-is-high-priority)
  - [II.a "Woah - way too much writing / many comments!"](#iia-woah---way-too-much-writing--many-comments)
  - [II.b Levels of Documentation Details](#iib-levels-of-documentation-details)
- [III. README.md Documentation](#iii-readmemd-documentation)
- [IV. Notebook Documentation](#iv-notebook-documentation)
- [V. Function Documentation](#v-function-documentation)
- [VI. Git: Commits and Branches](#vi-git-commits-and-branches)
  - [VI.a git commit](#via-git-commit)
  - [VI.b git branch](#vib-git-branch)
  - [VI.c ONLY OLIVER WILL USE THE MASTER BRANCH!](#vic-only-oliver-will-use-the-master-branch)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## I. Software Carpentry: Documention Station!!

Designing and implementing analytical workflows and software development is often a **complect** task in collaborative research projects. 

*Note: "complect" is defined as an object with components that are highly intertwined.* For example, our research project involves stitching together various data sources, statistical methods, and code sources that are highly complect (*...or complectity?...*).

Our tasks for this / any research project should be highly **accessible, reusable, and readable** code sources and workflows.

---

## II. Documentation is High Priority!

Cannot stress this enough...

### II.a "Woah - way too much writing / many comments!"

**WRONG!** 

*Over-documenting is never a problem.* 

*Under-documenting is always a problem.*  

### II.b Levels of Documentation Details

1. README.md: most general overview of code and workflow 
1. Notebooks: more specific, ~abstract of each at top 
1. Functions: even more specific, readable comments
1. Git commits: *$ git commit -m "most specific docs"*

---

## III. README.md Documentation

Brief description of each workflow step + Notebooks. 

1. Describe project logic 
1. Describe (briefly) each Notebook and code files

<br>

We need the least number of Notebooks and code files / sources that can subjectively described as high-level (*conceptual*) steps. 

For example, preprocessing and exploratory statistics of data can be logically grouped as a single notebook that **does not do anything past preprocessing and exploratory statistics**.

---

## IV. Notebook Documentation 

Each level of documentation should attempt to produce a file (-or- section of code software) that could be understood without looking at the rest of the code and workflow developed and explain what the general reasoning is behind the step. 

The top of each Notebook should be a sort of abstract of what the Notebook is for and what step in the workflow of our research project. And, in the same sense, visualization functions that are used across many Notebooks should be contained within a single **visualization.py** file within the *icyutils Python package* - see below for more information on software packages and their structure.

---

## V. Function Documentation 

**Docstrings!!** 

Docstrings are comments that exist just under the definition of a function:

 ```
        def this_fn(arg1, arg2):
            """
                Returns a list of strings based on arg1 x arg2
                        arg1 | <data_type> e.g. String, List, DataFrame
                        arg2 | <data_type>
            """
            return list(arg1 x arg2)

``` 

---

## VI. Git: Commits and Branches

### VI.a git commit

```
        $ git commit -m "commit msgs are the most specific docs (data rates and charges may apply)"
```

<br>

### VI.b git branch

Branches are an **extremely icy** component of the *git* software. 

**"master" is the default branch each repo gets.** A branch is like a single clone of the entire repo (*i.e. all files, Notebooks, code, etc*).

If you are on the **master** branch, creating a new branch will clone the project at whatever state it is in and become a parallel version of the repo. 

```
        # Creating a new branch called "dev-noaa" for NOAA development 
        $ git branch -b dev-noaa
``` 

### VI.c ONLY OLIVER WILL USE THE MASTER BRANCH! 

Unless you all do not agree...

It will be too complicated / time-consuming to handle merge conflicts and all kinds of wonkiness if multiple developers are in parallel versions of dev functions, files, Notebooks, etc. 

The **master** branch will have the most conservative number of *commits* - only using it to push clean steps in developing the code sources / files / repo. Or pushes to *master* will be when a significant portion of the analytical workflow / results are obtained. 

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
